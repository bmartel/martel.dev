import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import PageAside from "../components/page-aside";
import PageMain from "../components/page-main";
import PageSection from "../components/page-section";
import FullName from "../components/full-name";
import JobTitle from "../components/job-title";
import Contact from "../components/contact";
import ContentSection from "../components/content-section";
import StickySection from "../components/sticky-section";
import StickyHeader from "../components/sticky-header";
import SectionTitle from "../components/section-title";
import SectionSubtitle from "../components/section-subtitle";
import SectionText from "../components/section-text";
import TwoColumnLayout from "../components/two-column-layout";
import SEO from "../components/seo";
import resume from "../content/resume.json";
import { H2, H3, H4, P, UL, LI, Separator } from "../components/elements";

const contact = {
  phone: process.env.PHONE,
  email: process.env.EMAIL,
  github: process.env.GITHUB,
  linkedin: process.env.LINKEDIN,
  address: process.env.ADDRESS || "",
};

export const query = graphql`
  query {
    site {
      siteMetadata {
        description
        title
        keywords
      }
    }
  }
`;

const ResumePage = ({ data }) => {
  return (
    <Layout>
      <SEO title={resume.title} keywords={data.site.siteMetadata.keywords} />

      {resume.pages.map((page) => (
        <TwoColumnLayout key={page.id}>
          <PageAside>
            <PageSection>
              <StickySection>
                <FullName content={page.fullName} />
                <JobTitle content={page.jobTitle} />
              </StickySection>
              {page.sections.aside.map(
                ({ id, sectionTitle, title, subtitle, caption, content }) => (
                  <ContentSection key={id}>
                    <SectionTitle content={{ title, sectionTitle }} />
                    {id === "contact" ? (
                      <Contact key={id} content={contact} />
                    ) : (
                      <>
                        {subtitle ? (
                          <SectionSubtitle content={subtitle} />
                        ) : null}
                        {caption ? <SectionText content={caption} /> : null}
                        <SectionText content={content} />
                      </>
                    )}
                  </ContentSection>
                )
              )}
            </PageSection>
          </PageAside>
          <PageMain>
            <PageSection>
              <StickyHeader
                style={{
                  background: "var(--white)",
                  paddingTop: "var(--space-sm)",
                  paddingBottom: "var(--space-sm)",
                }}
              >
                <H2 style={{ margin: 0 }}>{page.sections.main.sectionTitle}</H2>
              </StickyHeader>
              {page.sections.main.jobs.map((job) => (
                <ContentSection key={job.id}>
                  <H3>{job.title}</H3>
                  <H4>
                    {job.company} <Separator>{job.date}</Separator>
                  </H4>
                  <P>{job.content}</P>
                  <UL>
                    {job.highlights.map((highlight) => (
                      <LI key={highlight.id}>{highlight.content}</LI>
                    ))}
                  </UL>
                </ContentSection>
              ))}
            </PageSection>
          </PageMain>
        </TwoColumnLayout>
      ))}
    </Layout>
  );
};

export default ResumePage;
