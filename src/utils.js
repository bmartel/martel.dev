import React from "react";

export const withAttrs = (Component, content) => {
  if (typeof content === "object") {
    const { text, ...props } = content;
    return <Component {...props}>{text}</Component>;
  }
  return <Component>{content}</Component>;
};
