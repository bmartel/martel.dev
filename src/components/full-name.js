import { H1 } from "./elements";
import { withAttrs } from "../utils";

const FullName = ({ content }) => {
  return withAttrs(H1, { style: { marginTop: 0 }, text: content });
};

export default FullName;

