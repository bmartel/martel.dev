import React from "react";
import PropTypes from "prop-types";
import { createGlobalStyle } from "styled-components";
import { StaticQuery, graphql } from "gatsby";
import { MDXProvider } from "@mdx-js/react";
import { H1, H2, H3, H4, H5, H6, P, UL, LI } from "./elements";

const GlobalStyle = createGlobalStyle`
  :root {
    --black: #10181c;
    --near-black: #1f3038;
    --dark-gray: #2e4652;
    --gray: #576b75;
    --light-gray: #82959e;
    --off-white: #ffffff;
    --near-white: #ffffff;
    --white: #ffffff;
    --primary: #fbcaaf;

    --text-base-size: 1em;
    --text-scale-ratio: 1.2;
    --text-xs: calc((1em / var(--text-scale-ratio)) / var(--text-scale-ratio));
    --text-sm: calc(var(--text-xs) * var(--text-scale-ratio));
    --text-md: calc(var(--text-sm) * var(--text-scale-ratio) * var(--text-scale-ratio));
    --text-lg: calc(var(--text-md) * var(--text-scale-ratio));
    --text-xl: calc(var(--text-lg) * var(--text-scale-ratio));
    --text-xxl: calc(var(--text-xl) * var(--text-scale-ratio));
    --text-xxxl: calc(var(--text-xxl) * var(--text-scale-ratio));
    --text-xxxxl: calc(var(--text-xxxl) * var(--text-scale-ratio));

    --space-unit:  1em;
    --space-xxxxs: calc(0.125 * var(--space-unit));
    --space-xxxs:  calc(0.25 * var(--space-unit));
    --space-xxs:   calc(0.375 * var(--space-unit));
    --space-xs:    calc(0.5 * var(--space-unit));
    --space-sm:    calc(0.75 * var(--space-unit));
    --space-md:    calc(1.25 * var(--space-unit));
    --space-lg:    calc(2 * var(--space-unit));
    --space-xl:    calc(3.25 * var(--space-unit));
    --space-xxl:   calc(5.25 * var(--space-unit));
    --space-xxxl:  calc(8.5 * var(--space-unit));
    --space-xxxxl: calc(13.75 * var(--space-unit));
  }
  html {
    box-sizing: border-box;
    -webkit-print-color-adjust: exact;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    margin: 0;
    font-size: var(--text-base-size);
    color: var(--near-black);
      --text-base-size: 1em;
      --space-unit:  1em;
  }
`;

function MDXLayout({ children }) {
  return (
    <MDXProvider
      components={{
        h1: H1,
        h2: H2,
        h3: H3,
        h4: H4,
        h5: H5,
        h6: H6,
        p: P,
        ul: UL,
        li: LI,
      }}
    >
      <GlobalStyle />
      {children}
    </MDXProvider>
  );
}

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={(data) => (
      <MDXLayout>
        {children}
      </MDXLayout>
    )}
  />
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
