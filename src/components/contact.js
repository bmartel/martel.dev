import React from "react";
import { P } from "./elements";

const Contact = ({ content }) => {
  return (
    <>
      <P>{content.phone}</P>
      <P>{content.email}</P>
      <P>{content.github}</P>
      <P>{content.linkedin}</P>
      <P>
        {content.address.split("\n").map((line) => (
          <div>{line}</div>
        ))}
      </P>
    </>
  );
};

export default Contact;
