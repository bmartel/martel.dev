import styled from "styled-components";

export default styled.aside`
  width: 100%;
  max-width: 320px;
  background-color: var(--dark-gray) !important;

  p {
    --dark-gray: var(--off-white) !important;
  }
  h2,
  h3,
  h4,
  h5,
  h6 {
    margin: var(--space-md) 0;
    font-weight: 400 !important;
    letter-spacing: 0.025em !important;
    --dark-gray: var(--off-white) !important;
    --near-black: var(--near-white) !important;
  }
   h2 {
    padding-top: var(--space-lg);
  }
  h1,
  h2 {
    color: var(--primary) !important;
  }
`;
