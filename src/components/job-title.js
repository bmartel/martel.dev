import { H3 } from "./elements";
import { withAttrs } from "../utils";

const JobTitle = ({ content }) => {
  return withAttrs(H3, { style: { marginBottom: 0 }, text: content });
};

export default JobTitle;
