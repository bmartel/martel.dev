import React from 'react';
import { H2, H3 } from "./elements";
import { withAttrs } from "../utils";

const SectionTitle = ({ content }) => {
  return (
    <>
      {content.sectionTitle ? withAttrs(H2, content.sectionTitle) : null}
      {content.title
        ? content.sectionTitle
          ? withAttrs(H3, content.title)
          : withAttrs(H2, content.title)
        : null}
    </>
  );
};

export default SectionTitle;
