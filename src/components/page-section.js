import styled from "styled-components";

export default styled.section`
  padding: var(--space-sm) var(--space-lg);
`;
