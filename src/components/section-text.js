import { P } from './elements';
import { withAttrs } from "../utils";

const SectionText = ({ content }) => {
  return withAttrs(P, content);
};

export default SectionText;
