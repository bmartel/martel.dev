import styled from "styled-components";

export default styled.div`
  padding: calc(2 * var(--space-sm)) 0;
  margin-left: -1.5px;
  width: 100%;
  background: var(--dark-gray);
  top: 0;
  position: sticky;
`;
