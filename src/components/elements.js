import styled from "styled-components";

const headingFont = `
  font-family: 'Playfair Display';
  color: var(--black);
  line-height: 1;
`;
const bodyFont = `
  font-family: 'Open Sans';
  color: var(--near-black);
  line-height: 1;
`;

export const H1 = styled.h1`
  ${headingFont}
  font-size: var(--text-xxl);
  margin: var(--space-sm) 0;
  text-transform: uppercase;
  letter-spacing: 0.1em;
  line-height: 1.25;
`;
export const H2 = styled.h2`
  ${headingFont}
  margin: var(--space-sm) 0;
  font-size: var(--text-md);
  text-transform: uppercase;
  letter-spacing: 0.025em;
  // line-height: 2.65;
  // line-height: 1.65;
  color: var(--gray);
`;
export const H3 = styled.h3`
  ${bodyFont}
  margin: var(--space-xxs) 0;
  font-size: var(--text-md);
  letter-spacing: -0.015em;
`;
export const H4 = styled.h4`
  ${headingFont}
  margin: var(--space-xxs) 0;
  font-size: var(--text-sm);
  font-weight: 300;
  font-style: italic;
  color: var(--dark-gray);
`;
export const H5 = styled.h5`
  ${headingFont}
  margin: var(--space-xxs) 0;
`;
export const H6 = styled.h6`
  ${headingFont}
  margin: var(--space-xxs) 0;
`;
export const P = styled.p`
  ${bodyFont}
  line-height: 1.45;
  font-size: var(--text-sm);
  margin: var(--space-sm) 0;
  color: var(--dark-gray);
`;
export const Separator = styled.span`
  ${bodyFont}
  padding-left: var(--space-xxs);
`;
export const Small = styled.small`
  ${bodyFont}
  font-size: var(--text-sm);
`;
export const UL = styled.ul`
  ${bodyFont}
  line-height: 1.45;
  font-size: var(--text-sm);
  padding-left: var(--space-lg);
  margin: var(--space-md) 0;
`;
export const LI = styled.li`
  ${bodyFont}
  color: var(--dark-gray);
  line-height: 1.45;
`;
