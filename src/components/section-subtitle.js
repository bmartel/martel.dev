import { H4 } from "./elements";
import { withAttrs } from "../utils";

const SectionSubtitle = ({ content }) => {
  return withAttrs(H4, content);
};

export default SectionSubtitle;
