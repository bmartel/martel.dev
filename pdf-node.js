#!/usr/bin/env node

const puppeteer = require("puppeteer");

// Taken and modified for use in gatsby from https://github.com/jsonresume/resume-cli/blob/master/lib/export-resume/index.js

function createPdf(
  url,
  fileName,
  format = ".pdf",
  pdfRenderOptions = {},
  callback
) {
  (async () => {
    const puppeteerLaunchArgs = [];

    // if (process.env.RESUME_PUPPETEER_NO_SANDBOX) {
    puppeteerLaunchArgs.push("--no-sandbox");
    puppeteerLaunchArgs.push("--headless");
    // }

    const browser = await puppeteer.launch({
      args: puppeteerLaunchArgs,
    });
    const page = await browser.newPage();
    page.setExtraHTTPHeaders({ "upgrade-insecure-requests": "0" });
    await page.emulateMedia('screen');
    await page.goto(url, { waitUntil: "networkidle2" });
    await page.pdf({
      path: fileName + format,
      format: "A4",
      printBackground: true,
    });

    await browser.close();
  })()
    .then(callback)
    .catch(callback);
}

function exportPdf(url, fileName, callback) {
  const format = ".pdf";
  createPdf(url, fileName, format, {}, function(error) {
    if (error) {
      console.error(error, "`createPdf` errored out");
    }
    callback(error, fileName, format);
  });
}

if (process.argv.length) {
  console.log(process.argv);
  let [, , urls, fileNames = "", ...pdfArgs] = process.argv;
  fileNames = fileNames.split(",");
  urls.split(",").map((url, index) =>
    exportPdf(url, fileNames[index] || `pdf_${index}`, function(error) {
      if (error) {
        console.error(error, "`createPdf` errored out");
      }
    })
  );
}
